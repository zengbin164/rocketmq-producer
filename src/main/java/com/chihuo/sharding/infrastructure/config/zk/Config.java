package com.chihuo.sharding.infrastructure.config.zk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.CuratorCache;
import org.apache.curator.framework.recipes.cache.CuratorCacheListener;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
	
	private static final Logger log = LoggerFactory.getLogger(Config.class);
	
	private Map<String, String> cache = new HashMap<>();
	private CuratorFramework client;
	private static final String CONFIG_PREFIX = "/config";
	private static final String ZK_ADDRESS = "127.0.0.1:2181";

	// 初始化zk连接
	public Config() {
		String zkAdd = ZK_ADDRESS;
		this.client = CuratorFrameworkFactory.newClient(zkAdd, new RetryNTimes(3, 1000));
		this.client.start();
		this.init();
	}

	public void init() {
		try {
			// 从zk中获取配置项并保存到缓存中
			List<String> childrenNames = client.getChildren().forPath(CONFIG_PREFIX);
			for (String name : childrenNames) {
				byte []bytes = client.getData().forPath(CONFIG_PREFIX + "/" + name);
				String value = null;
				if(ArrayUtils.isEmpty(bytes)) {
					log.warn("-----Zookeeper 获取值为空，路径为 [{}]", CONFIG_PREFIX + "/" + name);
				} else {
					value = new String(bytes);
				}
				cache.put(name, value);
			}
			CuratorCache curatorCache = CuratorCache.builder(client, CONFIG_PREFIX).build();
			CuratorCacheListener listener = CuratorCacheListener.builder().forPathChildrenCache(CONFIG_PREFIX, client, new PathChildrenCacheListener() {
				@Override
				public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent event) throws Exception {
					String path = event.getData().getPath();
					if (path.startsWith(CONFIG_PREFIX)) {
						String key = path.replace(CONFIG_PREFIX + "/", "");
						// 子节点新增或变更时 更新缓存信息
						if (PathChildrenCacheEvent.Type.CHILD_ADDED.equals(event.getType()) || PathChildrenCacheEvent.Type.CHILD_UPDATED.equals(event.getType())) {
							cache.put(key, new String(event.getData().getData()));
						}
						// 子节点被删除时 从缓存中删除
						if (PathChildrenCacheEvent.Type.CHILD_REMOVED.equals(event.getType())) {
							cache.remove(key);
						}
					}
				}
			}).build();
			curatorCache.listenable().addListener(listener);
			curatorCache.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 保存配置信息
	public void save(String name, String value) {
		String configFullName = CONFIG_PREFIX + "/" + name;
		try {
			Stat stat = client.checkExists().forPath(configFullName);
			if (stat == null) {
				client.create().creatingParentsIfNeeded().withMode(CreateMode.PERSISTENT).forPath(configFullName, value.getBytes());
			} else {
				client.setData().forPath(configFullName, value.getBytes());
			}
			cache.put(name, value);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 获取配置信息
	public String get(String name) {
		return cache.get(name);
	}
	
    public static void main(String[] args) {
        Config config = new Config();
        // 模拟一个配置项，实际生产中会在系统初始化时从配置文件中加载进来
        config.save("timeout", "1000");
		
		// 每3S打印一次获取到的配置项
        for (int i = 0; i < 10; i++) {
            System.out.println(config.get("timeout"));
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
